#!/usr/bin/env python
# pylint: disable=W0613, C0116
# type: ignore[union-attr]
# This program is dedicated to the public domain under the CC0 license.


# Include all dependencies
import logging, sys, requests, time, math

from time import gmtime
from time import strftime

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update, ParseMode
from telegram.ext import (Updater,CommandHandler,MessageHandler,Filters,ConversationHandler,CallbackContext,)

bot_token = sys.argv[1]

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

# Initialize all States
STATE_CHOICE = "wait_for_choice"
STATE_RESTAURANT = "wait_for_restaurant"
STATE_GO_OUT = "wait_for_go_out"
STATE_LOCATION = "wait_for_location"
STATE_LIST_RESTAURANT = "wait_for_list_restaurant"
STATE_DESCRIPTION_RESTAURANT = "wait_for_a_description"
STATE_MUSEUMS_RECOMMANDATION = "wait_for_museum_recommandation"
STATE_BARS_RECOMMANDATION = "wait_for_bars_recommandation"
STATE_CLUBS_RECOMMANDATION = "wait_for_clubs_recommandation"
STATE_TRANSPORT = "wait_for_transport"

# ************************ Functions ************************
#START : WELCOME + CHOICE
def start(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [
        ['Go out 🏊', 'Restaurant 🍛'],  #choix principal
        ['exit 🎈'],
    ]

    update.message.reply_text(
        'Welcome to *Geneva guide* \! \n\n'
        'What do you prefer', parse_mode='MarkdownV2',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return STATE_CHOICE

#PRINT A RESTAURANTS STYLE LIST
def restaurant(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [
         ['Italian 🍕', 'French 🥩'],
         ['Japanese 🍣', 'American 🍔', 'Mexican 🌶'],
         ['return ⬅️', 'exit 🎈'],
    ]

    user = update.message.from_user
    logger.info("So %s: %s what Restaurant do you want?", user.first_name, update.message.text)
    update.message.reply_text(
        'Do a choice :) ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )

    return STATE_RESTAURANT

#GOUUT LIST ACTIVITIES
def go_out(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [
        ['Museums 🏛', 'Bars 🍻', 'Clubs 🥳'],
        ['return ⬅️', 'exit 🎈'],
    ]
    update.message.reply_photo("https://images.unsplash.com/photo-1599508704512-2f19efd1e35f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=675&q=80")

    update.message.reply_text(
        'Do you want go out? Select one : ',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )
    return STATE_GO_OUT

#GOUUT LIST ACTIVITIES
def recommandation_museums(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [
        ['return ⬅️', 'menu ✵'],
        ['exit 🎈'],
    ]

    update.message.reply_text("test", reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    update.message.reply_text(" Top 3 : \n\n 1⃣ - Ethnographic Museum\nBoulevard Carl-Vogt 65, 1205 Geneva, Switzerland\n4.7/5 197 reviews\n")
    update.message.reply_text("<a>https://www.ville-ge.ch/meg/</a>", parse_mode='HTML')
    update.message.reply_text("Discover a beautiful museum that give you the chance of take a good vision of the other cultures of the world!")
    update.message.reply_photo("http://www.ville-ge.ch/meg/img/94.jpg")

    update.message.reply_text("\n 2⃣- CERN museum\n<a>Route du cern, 1212 Route de Meyrin 23,Geneva, Switzerland</a>\n4.7/5 1500 reviews\n", parse_mode='HTML')
    update.message.reply_text("<a>https://visit.cern/</a>", parse_mode='HTML')
    update.message.reply_text("The cern give you the chance of to entry into the Cern experiences, from physics to computer sciences, discover all of this monument of sciences!")
    update.message.reply_photo("https://media-cdn.tripadvisor.com/media/photo-s/0c/a7/6a/dc/photo2jpg.jpg")

    update.message.reply_text("\n 3⃣ - Museum of Natural History (Museum d'Histoire naturelle)\n route de Malagnou 1, Geneva 1208 Switzerland\n4.5/5 682 reviews")
    update.message.reply_text("<a>http://institutions.ville-geneve.ch/fr/mhn/</a>", parse_mode='HTML')
    update.message.reply_text("Visit the most important biologist center of switzerland! An incountournable visit for childrens. ")
    update.message.reply_photo("https://img.over-blog-kiwi.com/1/40/66/69/20150213/ob_727232_musee-d-histoire-naturelle-de-geneve.jpg")

    return STATE_MUSEUMS_RECOMMANDATION


def recommandation_bars(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [
        ['return ⬅️', 'menu ✵'],
        ['exit 🎈'],
    ]

    update.message.reply_text("test", reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    update.message.reply_text(" Top 3 : \n\n 1⃣ - Rollers Bar\nRuelle des Templiers 4, Geneva 1207 Switzerland\n4.7/5 23 reviews\n")
    update.message.reply_text("Located in Eaux Vives, Geneva, Rollers is Switzerland's first indoor mini-golf cocktail bar. Rollers brings the lights, cameras and action back to one of Geneva's classic underground nightclubs. ")
    update.message.reply_text("<a>https://www.rollersbar.ch</a>", parse_mode='HTML')
    update.message.reply_photo("https://images.squarespace-cdn.com/content/v1/5b15108eaa49a1a8a6b2ae80/1558978605356-Q2VTD6RJULUARDC3GZJQ/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/Rollers+Bar+Rive+5.JPG?format=2500w")

    update.message.reply_text("\n 2⃣- Little Barrel\n<a>rue du Lac 15, Geneva 1207 Switzerland</a>\n4.7/5 45 reviews\n", parse_mode='HTML')
    update.message.reply_text("LITTLE BARREL is a discreet and authentic bar just a few steps away from the lake. Quentin Beurgaud & Nicolas Berger are always there to provide you with a personalized experience by advising you on a unique selection of 100 rums and many cocktails adapted to the richness and exoticism of the old cane liquor.")
    update.message.reply_photo("https://media-cdn.tripadvisor.com/media/photo-s/12/aa/64/45/little-barrel-bar.jpg")

    update.message.reply_text("\n 3⃣ -L'Eléphant dans La Canette\n avenue du Mail 18, Geneva 1205 Switzerland\n4.5/5 682 reviews")
    update.message.reply_text("L'elephant dans la canette is a attractive  bar just a few steps away from plainpalais. Food is excellent, and beer freezing!")
    update.message.reply_text("<a>https://elephantdanslacanette.ch/</a>", parse_mode='HTML')

    return STATE_BARS_RECOMMANDATION

def recommandation_clubs(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [
        ['return ⬅️', 'menu ✵'],
        ['exit 🎈'],
    ]

    update.message.reply_text("test", reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    update.message.reply_text(" Top 2 : \n\n 1⃣ -  Village du Soir\nRoute des Jeunes 24, Geneva 1212 Switzerland\n4.7/5 99 reviews\n")
    update.message.reply_text("A unique place in Geneva with three spaces dedicated to sound: La Carrosserie La Distillerie L'Epicerie On a unique site, the best of the region's bars on an ephemeral basis. Reference for Geneva after-work ")
    update.message.reply_text("<a>https://www.villagedusoir.com/</a>", parse_mode='HTML')
    update.message.reply_photo("https://mybiggeneva.com/wp-content/uploads/2019/10/14882197_1606870209608825_522266660406944858_o.jpg")

    update.message.reply_text("\n 2⃣- Le Terreau\n<a>rue du Lac 15, Geneva 1207 Switzerland</a>\n4.7/5 45 reviews\n", parse_mode='HTML')
    update.message.reply_text("Le Terreau has been a nightlife venue in Geneva since 2015 which offers all types of events every week at student prices. See our Facebook page for more information.")
    update.message.reply_text("<a>https://collectif-nocturne.ch/salle-du-terreau//</a>", parse_mode='HTML')
    update.message.reply_photo("https://media-cdn.tripadvisor.com/media/photo-s/12/aa/64/45/little-barrel-bar.jpg")


    return STATE_CLUBS_RECOMMANDATION


# LIST OF RESTAURANTS
def print_restaurants(update: Update, context: CallbackContext) -> int:
    dictRestaurants= {  #We save the restaurants information into a dictionnary
        "rest1": {
            "name": "Au parfait",
            "type": "french",
            "price": "💲💲💲💲",
        },
        "rest2": {
            "name": "Burgerzz",
            "type": "American",
            "price": "💲",
        },
        "rest3": {
            "name": "Tanuki",
            "type": "Japanese",
            "price": "💲💲💲",
        }
    }
    #adapt automatically the buttons name from the dictionnary
    reply_keyboard = [
        [dictRestaurants["rest1"]["name"], dictRestaurants["rest2"]["name"], dictRestaurants["rest3"]["name"]],
        ['return', 'exit'],
    ]
    parse_mode = ParseMode.MARKDOWN_V2
    update.message.reply_text('*Our top of this category :*', parse_mode='MarkdownV2')
    for restaurant in dictRestaurants.values(): #adapt automatically the emoticon style from the key "type" of the restaurant dictionnary
        if restaurant["type"] == "french":
            emoticon = ' 🥩'
        elif restaurant["type"] == "Japanese":
            emoticon = ' 🍣'
        elif restaurant["type"] == "American":
            emoticon = ' 🍔'
        else:
            emoticon = ' 🍻'

        restaurant_to_print = " name : " + restaurant["name"] +"\n" +  'style : ' + restaurant["type"] +"\n"  + emoticon + 'price : ' + restaurant["price"] + "\n" + '--------------------------------'

        update.message.reply_text(
            restaurant_to_print,
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
        )

    return STATE_LIST_RESTAURANT

#DESCRIPTION RESTAURANT
def description_restaurant(update: Update, context: CallbackContext) -> int:
    reply_keyboard = [
        ['return ⬅️', 'menu ✵'],
        ['exit 🎈'],
    ]
    description = "Welcome, our restaurant purpose good Japanese Specialities! \n Our chief Mr. Kawasaki work hard to do the best for customers."
    phone_number = "022/474.44.11"
    website = "<a>http://www.tanuki-sushi.ch/</a>"
    longitude = 46.1942143
    latitude = 6.1405377

    update.message.reply_text("*Tanuki*", parse_mode='MarkdownV2', reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    update.message.reply_text(description + "\n" + phone_number + "\n" + website, parse_mode='HTML')

    """update.message.reply_text("Welcome, our restaurant purpose good Japanese Specialities! \n Our chief Mr. Kawasaki work hard to do the best for customers.\n ")
    update.message.reply_text("<a>http://www.tanuki-sushi.ch/</a>", parse_mode='HTML')
    update.message.reply_text("022/474.44.11")
    update.message.reply_text("Find us!")"""
    update.message.reply_location(longitude, latitude)

    return STATE_DESCRIPTION_RESTAURANT

#TRANSPORT IMPLEMENTATION
# Fonctions communes
def appeler_opendata(path):
    url = "https://transport.opendata.ch/v1/" + path
    result = requests.get(url)
    print(url)
    return result.json()

def afficher_arrets(arrets, update):
    for arret in arrets['stations']:
        if arret['id'] is not None:
            update.message.reply_text("/stop" + arret['id'] + " - " + arret['name'])
    return STATE_TRANSPORT

def calculer_temps(timestamp):
    seconds = timestamp - time.time()
    minutes = seconds/60
    minutes = math.floor(minutes)

    if minutes < 0:
        return "Trop tard"

    if(minutes < 3):
        return "Presque parti"

    return "{} min".format(minutes)

# Récupération des actions à faire

def message_de_bienvenue(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Bonjour, veuillez m'envoyer votre localisation (en texte ou en pièce jointe)")
    return STATE_TRANSPORT

def rechercher_par_localisation(update: Update, context: CallbackContext) -> None:
    location = update.message.location
    arrets = appeler_opendata("locations?x={}&y={}".format(location.longitude, location.latitude))
    afficher_arrets(arrets, update)
    return STATE_TRANSPORT

def rechercher_par_nom(update: Update, context: CallbackContext) -> None:
    arrets = appeler_opendata("locations?query=" + update.message.text)
    print(arrets)
    afficher_arrets(arrets, update)
    return STATE_TRANSPORT

def afficher_horaires(update: Update, context: CallbackContext) -> None:
    arret_id = update.message.text[5:]
    prochains_departs = appeler_opendata("stationboard?id={}&limit=10".format(arret_id))

    reponse = "Voici les prochains départs:\n\n"

    for depart in prochains_departs["stationboard"]:
        stop = depart['stop']
        reponse = reponse + calculer_temps(stop['departureTimestamp']) + ' ' + depart['number'] + " -> " + depart['to'] + "\n"

    reponse = reponse + "\nPour actualiser: /stop{}".format(arret_id)
    update.message.reply_text(reponse)
    return STATE_TRANSPORT

#Cancel
def cancel(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text(
        'Bye! I hope that helps! See you! ', reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END

# ************************ End Functions ************************
updater = Updater(bot_token)
dispatcher = updater.dispatcher

# CONVERSATION HANDLER
conv_handler_geneva = ConversationHandler(
    entry_points=[CommandHandler('start', start)],
    states={
        STATE_CHOICE: [
            MessageHandler(Filters.regex('^(Restaurant)'), restaurant),
            MessageHandler(Filters.regex('^(Go out)'), go_out),
            MessageHandler(Filters.regex('^(exit)$'), cancel),
        ],
        #restaurant way
        STATE_RESTAURANT: [
            MessageHandler(Filters.regex('^(Japanese|American|Mexican|French|Italian)'), print_restaurants),
            MessageHandler(Filters.regex('^(return)'), start),
            MessageHandler(Filters.regex('^(menu)'), start),
            MessageHandler(Filters.regex('^(exit)'), cancel),
        ],
        STATE_LIST_RESTAURANT: [
            MessageHandler(Filters.regex('^(return)'), restaurant),
            MessageHandler(Filters.regex('^(menu)'), start),
            MessageHandler(Filters.regex('^(exit)'), cancel),
            MessageHandler(Filters.regex('Au parfait|Burgerzz|Tanuki'), description_restaurant)
        ],
        STATE_DESCRIPTION_RESTAURANT: [
            MessageHandler(Filters.regex('^(return)'), print_restaurants),
            MessageHandler(Filters.regex('^(menu)'), start),
            MessageHandler(Filters.regex('^(exit)'), cancel),
            MessageHandler(Filters.regex('transport'), message_de_bienvenue),
        ],
        #go out way
        STATE_GO_OUT: [
            MessageHandler(Filters.regex('^(return)'), start),
            MessageHandler(Filters.regex('exit'), cancel),
            MessageHandler(Filters.regex('^(menu)'), start),
            MessageHandler(Filters.regex('Museums'), recommandation_museums),
            MessageHandler(Filters.regex('Bars'), recommandation_bars),
            MessageHandler(Filters.regex('Clubs'), recommandation_clubs),
        ],
        STATE_MUSEUMS_RECOMMANDATION: [
            MessageHandler(Filters.regex('^(return)'), go_out),
            MessageHandler(Filters.regex('^(menu)'), start),
            MessageHandler(Filters.regex('^(exit)'), cancel),
        ],
        STATE_BARS_RECOMMANDATION: [
            MessageHandler(Filters.regex('^(return)'), go_out),
            MessageHandler(Filters.regex('^(menu)'), start),
            MessageHandler(Filters.regex('^(exit)'), cancel),
        ],
        STATE_CLUBS_RECOMMANDATION: [
            MessageHandler(Filters.regex('^(return)'), go_out),
            MessageHandler(Filters.regex('^(menu)'), start),
            MessageHandler(Filters.regex('^(exit)'), cancel),
        ]
    },
    fallbacks=[
        CommandHandler('cancel', cancel),
        CommandHandler('transport', message_de_bienvenue),
    ],
)

# Add conversation transport
conv_handler_transport = ConversationHandler(
    entry_points=[CommandHandler('transport', message_de_bienvenue)],
    states={
        STATE_TRANSPORT: [
            CommandHandler('cancel', cancel),
            MessageHandler(Filters.location, rechercher_par_localisation),
            MessageHandler(Filters.command, afficher_horaires),
            MessageHandler(Filters.text, rechercher_par_nom),
        ],
    },
    fallbacks=[
        CommandHandler('cancel', cancel),
        CommandHandler('start', start),
    ],
)

dispatcher.add_handler(conv_handler_geneva)
dispatcher.add_handler(conv_handler_transport)
# Start the Bot
updater.start_polling()

# Run the bot until you press Ctrl-C or the process receives SIGINT,
# SIGTERM or SIGABRT. This should be used most of the time, since
# start_polling() is non-blocking and will stop the bot gracefully.
updater.idle()